/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:41:27 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 04:26:07 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*fresh;
	void	**tmp;

	if (!(fresh = (t_list*)malloc(sizeof(t_list))))
		return (NULL);
	tmp = &fresh->content;
	if (content && content_size)
	{
		if (!(*tmp = malloc(content_size)))
			return (NULL);
		ft_memcpy(*tmp, content, content_size);
		fresh->content_size = content_size;
	}
	else
	{
		*tmp = NULL;
		fresh->content_size = 0;
	}
	fresh->next = NULL;
	return (fresh);
}
