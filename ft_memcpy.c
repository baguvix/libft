/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:41:54 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:41:56 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*c_dst;
	char	*c_src;

	if (dst == src)
		return (dst);
	c_dst = (char*)(dst);
	c_src = (char*)(src);
	while (n--)
		*c_dst++ = *c_src++;
	return (dst);
}
