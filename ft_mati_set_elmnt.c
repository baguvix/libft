/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mati_set_elmnt.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/03 16:18:01 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/03 16:18:03 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_mati_set_elmnt(t_mati *a, size_t m, size_t n, int elmnt)
{
	if (!a || (a->m <= m) || (a->n <= n))
		return ;
	a->elem[m * a->n + n] = elmnt;
	return ;
}
