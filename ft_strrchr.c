/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:55:50 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:55:52 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*last;
	char	c_c;

	c_c = (char)(c);
	last = *s == c_c ? (char*)s : NULL;
	while (*s++)
		last = *s == c_c ? (char*)s : last;
	return (last);
}
